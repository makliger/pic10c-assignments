/
//  main.cpp
//  pic10c
//
//  Created by Michael Kliger on 3/3/19.
//  Copyright © 2019 Michael Kliger. All rights reserved.
#include <iostream>


namespace Pic10b
{
    
    template <typename T>
    class vector{
    private:
        T* data;
        size_t size;
        size_t capacity;
        static const int INIT_CAP = 10;
        
    public:
        // The big 4
        vector();
        vector( const vector& );
        vector& operator=( const vector& );
        ~vector();
        
        // Other members [public]
        bool empty() const{return size==0;};
        const size_t get_size()const {return size;};
        size_t get_capacity()const{return capacity;};
        T& operator[]( size_t index )const;
        T& back(){return *data;};
        T& front(){return *(data + size - 1);};
        T& index_of(size_t index) const;
        void dump_data_to( std::ostream& out )const{
            out << "Vector (dump): ";
            for ( int i = 0 ; i < capacity ; ++i )
                out << data[i] << ' ';
            out << '\n';
            out << "hello";
        };
        void dump_data() const;
        void push_back(T new_value );
        void pop_back();
        
    private:
        //Other members [private]
        void reserve( size_t new_capacity ){
            if ( new_capacity > capacity ) {
                if ( new_capacity <= 2 * capacity )
                    new_capacity = 2 * capacity;
                
                T* old_location = data;
                
                data = new T[new_capacity];
                capacity = new_capacity;
                
                for ( size_t i = 0 ; i < size ; ++i )
                    data[i] = old_location[i];
                
                delete old_location;
            }
        }
        
    }; // end Pic10b::vector
    
}

using namespace Pic10b;



template <typename T>
/** ************************* THE BIG 4 ************************* **/
vector<T>::vector()
: data(nullptr), size(0), capacity(INIT_CAP)
{
    data = new T[capacity];
    std::cout<< "xxxx this is the Default Constructor " << std::endl;
}
template <typename T>
vector<T>::vector( const vector<T>& source ) // CC
: data(nullptr), size(source.size),
capacity(source.capacity) {
    
    data = new T[capacity];
    // Deep copy of internal array
    for ( int i = 0 ; i < size ; ++i ){
        data[i] = source.data[i];
    }
    std::cout<< "xxxx this is the Copy Constructor " << std::endl;
}
template <typename T>
vector<T>& vector<T>::operator=( const vector<T>& rhs ) {
    if ( this != &rhs ) {     // Self-assignment?
        // Release old memory and request more
        delete[] data;
        data = new T[rhs.capacity];
        
        // Shallow copy non-pointers
        size = rhs.size;
        capacity = rhs.capacity;
        
        // Deep copy internal array
        for ( int i = 0 ; i < size ; ++i )
            data[i] = rhs.data[i];
    }
    std::cout<< "xxxx this is the Assignment Operator " << std::endl;
    return *this;
}
template <typename T>
vector<T>::~vector(){
    std::cout<< "xxxx this is the Destructor " << std::endl;
    delete[] data;
}

/** *********************** OTHER MEMBERS *********************** **/


template <typename T>
T& vector<T>::index_of( size_t index ) const
{
    if ( index < get_size() )
        return data + index;
}
template <typename T>
T& vector<T>::operator[]( size_t index )const
{
    if ( index < get_size() )
        return data[index];
    else
    {
        
        std::cout << "Pic10b vector : Index out of Bounds ";
        
    }
    return data[0];
}

template <typename T>
void vector<T>::dump_data() const
{
    dump_data_to( std::cout);
}

template <typename T>
void vector<T>::push_back( T new_value )
{
    if ( size == capacity)
        reserve( capacity + 1 );     // `data` is reassigned
    
    data[size++] = new_value;
}

// I dont agree with the professor here. If he was saying we shouldnt shrink it or not I am not sure
// IT is certainly best practices to free the resource we are deleting.

template <typename T>
void vector<T>::pop_back(){
    
    if ( size > 0 )
    {
        --size;
    }
    
}




/** ************************ OTHER FUNCTIONS ************************ **/
template <typename T>
std::ostream& operator<<( std::ostream& out, const Pic10b::vector<T>& v )
{
    int sz = v.get_size();
    for ( size_t i = 0 ; i < sz; ++i )
        out << v[i] << ' ';
    return out;
}

template <typename T>
void print_vector( const Pic10b::vector<T>& v ){
    if ( v.empty() )
        std::cout << "Vector is empty\n";
    else
        std::cout << "Vector (contents): " << v << '\n' ;
}

template<>
std::ostream& operator<<( std::ostream& out, const Pic10b::vector<std::string>& v )
{
    int sz = v.get_size();
    out << "[ ";
    
    for ( size_t i = 0 ; i < sz; ++i )
    {
        if(i != sz-1)
            out << v[i] << ", ";
        else
            out << v[i];
    }
    
    out << " ] ";
    return out;
}


Pic10b::vector<std::string> operator *( std::string lhs, const Pic10b::vector<std::string>& v )
{
    int sz = v.get_size();
    Pic10b::vector<std::string> *res = new Pic10b::vector<std::string>();
    
    for(int i =0; i < sz; i++)
    {
        res->push_back(lhs + ' ' + v[i]);
    }
    
    return *res;
    
}

Pic10b::vector<std::string> operator *( const Pic10b::vector<std::string>& v, std::string rhs )
{
    int sz = v.get_size();
    Pic10b::vector<std::string> *res = new Pic10b::vector<std::string>();
    
    for(int i =0; i < sz; i++)
    {
        res->push_back( v[i] + ' ' + rhs);
    }
    
    return *res;
    
}







