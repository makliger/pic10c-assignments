#ifndef VIEW_H
#define VIEW_H



#include <QWidget>

class QCheckBox;
class QComboBox;
class QGroupBox;
class QLabel;
class QSpinBox;
class QLineEdit;
class QSlider; // hw
class QScrollBar; // mid // may as well use three different kinds of controls
class QDial; // final


class View : public QWidget
{
    Q_OBJECT

public:
    View();
public slots:
    void setValueHW(int value);
    void setValueMid(int value);
    void setValueFin(int value);
    void setSchema(int value);

private:
    void createControls(const QString &title);
    QString calculateScore();

    bool weight; // 0 for default
    qint8 hwScore;
    qint8 midtermScore;
    qint8 finalScore;

    QList<double> weightOne;
    QList<double> weightTwo;

    QLineEdit *display;
    QGroupBox *controlsGroup;
    QGroupBox *slidersGroup;
    QComboBox *orientationCombo;

    QSlider *slider; // hw
    QScrollBar *scrollBar; // mid // may as well use three different kinds of controls
    QDial *dial; // final
};

#endif // VIEW_H
