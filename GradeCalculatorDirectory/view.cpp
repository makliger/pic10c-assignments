

#include <QtWidgets>

#include "pic10cwidget.h"
#include "view.h"



View::View()
{

    weight= true; // true for default
     hwScore =0;
     midtermScore =0;
     finalScore = 0;
    // create controls for scores
    slider = new QSlider(Qt::Vertical);   // got most of these settings from QT docs, dont think all are necessary
    slider->setFocusPolicy(Qt::StrongFocus);
    slider->setTickPosition(QSlider::TicksBothSides);
    slider->setValue(0);
    slider->setRange(0,100);

    scrollBar = new QScrollBar(Qt::Vertical);
    scrollBar->setFocusPolicy(Qt::StrongFocus);
    scrollBar->setRange(0,100);
    scrollBar->setValue(0);

    dial = new QDial;
    dial->setFocusPolicy(Qt::StrongFocus);
    dial->setRange(0,100);
    dial->setValue(0);

    connect(slider, SIGNAL(valueChanged(int)), this, SLOT(setValueHW(int)));
    connect(scrollBar, SIGNAL(valueChanged(int)), this, SLOT(setValueMid(int)));
    connect(dial, SIGNAL(valueChanged(int)), this, SLOT(setValueFin(int)));


    /*==================================
     */
    // create layout groups


    display = new QLineEdit(tr("HW/MT/F/T:"));

    orientationCombo = new QComboBox;
    orientationCombo->addItem(tr("Schema 1 : 20 - 35 - 45"));
    orientationCombo->addItem(tr("Schema 2 : 35 - 20 - 35"));

    connect(orientationCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(setSchema(int)));

    QHBoxLayout *controlsLayout = new QHBoxLayout();
    controlsLayout->addWidget(display);
    QHBoxLayout *schemaLayout = new QHBoxLayout();
    schemaLayout->addWidget(orientationCombo);
    //controlsGroup->setLayout(controlsLayout);

    /*==================================
     */

    QBoxLayout *slidersLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    slidersLayout->addWidget(slider);
    slidersLayout->addWidget(scrollBar);
    slidersLayout->addWidget(dial);
  //  slidersGroup->setLayout(slidersLayout);


    QVBoxLayout *layout = new QVBoxLayout;
    layout->addLayout(controlsLayout);
    layout->addLayout(slidersLayout);
    layout->addLayout(schemaLayout);
    setLayout(layout);

    weightOne = {20.0, 35.0, 45.0}; // weights based on binary choice
    weightTwo = {35.0, 20.0, 35.0};


    setWindowTitle(tr("Grade calculator"));
}

void View::setValueHW(int value)
{
    hwScore = value;
    display->setText(calculateScore());


}
void View::setValueMid(int value)
{
    midtermScore = value;
    display->setText(calculateScore());
}
void View::setValueFin(int value)
{


    finalScore= value;
   display->setText(calculateScore());

}
void View::setSchema(int value)
{
    if(value == 0)
    {
        weight = true;
    }
    else
     weight = false;
}

void View::createControls(const QString &title)
{

}
QString View::calculateScore()
{
    QString str = "";
    if(weight)
    {// algorithm for displaying score string, QString.arg only takes 3 paramters for some reason
        double fin = (weightOne.value(1) * midtermScore/100.0);
        fin += (weightOne.value(0) * hwScore/100.0);
        fin += (weightOne.value(2) * finalScore/100.0);
        int finale = fin; // dont want any decimal precision
        str = QString::number(weightOne.value(0) * hwScore/100.0);
        str += " " + QString::number(weightOne.value(1) * midtermScore/100.0);
        str += " " + QString::number(weightOne.value(2) * finalScore/100.0);
        str += " " + QString::number(finale);
        /*
        str = QString("HW(%1):%2, MT(%3)").arg(weightOne.value(0), hwScore, weightOne.value(1));
        str += QString(": %1, F(%2), %3, T: ").arg( midtermScore,  weightOne.value(2), finalScore);
        str += QString("%1").arg(finale);*/
    }
    else
    {
        double fin = (weightTwo.value(1) * midtermScore/100.0);
        fin += (weightTwo.value(0) * hwScore/100.0);
        fin += (weightTwo.value(2) * finalScore/100.0);
        int finale = fin; // dont want any decimal precision

        str = QString::number(weightTwo.value(0) * hwScore/100.0);
        str += " " + QString::number(weightTwo.value(1) * midtermScore/100.0);
        str += " " + QString::number(weightTwo.value(2) * finalScore/100.0);
        str += " " + QString::number(finale);
        /*
        str = QString("HW(%1):%2, MT(%3)").arg(weightTwo.value(0), hwScore, weightTwo.value(1));
        str += QString(": %1, F(%2), %3, T: ").arg( midtermScore,  weightTwo.value(2), finalScore);
        str += QString("%1").arg(finale);*/


    }
    return str;

}
